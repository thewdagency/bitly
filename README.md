# Bitly Test

This is a test site provided by Bitly. 

## Requirements

* Node v16+
* Bitly API key

## Install

To install this application pull down the repo. Once you have it pulled down you will need to run `npm install` in order to load the required NPM packages. Then you will need to create a '.env' file as a copy from the '.env.sample' file, and replace the '*'s with your own Bitly API key. Once that is completed you will be able to run the site using `npm start`.

### Notes:

- Environment
  - The environment used is based upon React 17, Webpack 5, and Material UI. While I am no expert with Material UI I thought this was a good way to put a single page app up quickly. Hind-sight being 20/20 I believe I should have used something I was a little more familiar with like 'react-bootstrap'. While the file structure is a bit more simplified when it comes to requiring CSS files this took a little longer to execute. I did, however, see that there is huge potential with Material UI's v5 theme provider
  - The menubar is based upon the Material UI App Bar. This is the first time I have used this, but it seems to work very well for responsiveness since the menu is simplistic
  - Since this is a simple single page app I decided to store the link data into the session storage. This allows for a semi-persistent way of showing the input link data
  - Errors/Notifications are handled via the Snackbar functionality of Material UI
    - I believe there are better ways of doing this (as well as changing the colors based upon the message type) but this was a quick way to get the messages pushed to the screen while keeping it mobile friendly.
    - One example of these notifications is that if the entered value is not considered a valid URL, it will spit out the error message for you
  - The logo image did not seem correct. The letter 'b' is filled in, but I just wanted to add this to the notes
  - Helvetica is the base font
- The Bitly API was not fully functioning, so I could not get anything beyond the 'shorten()' endpoint
  - To get around this we are basically adding data on the fly based upon the entered URL. Since this data is stored in the browser session, in order to reset it you could close the tab/window and visit the page again, or you can go into the browsers dev panel under 'Applications' and remove the SESSION data for the page
- My own issues
  - The way Material UI lets you add CSS is not something I prefer. While it does help with load times since it creates a kind of 'in-line' approach, it does not allow for the components to be very easily read. I think I would prefer a compilable (modular) SCSS format over this
  - Also dealing with Material UI, some components do not allow for consistent style usage. Even though there is a theme you would have to add some aspects of your styles in-line in order for them to work. Specifically, this is the case for form fields
  - The main.js Chunk file is about 7MB.  Seems pretty large for such a small page. I think we could improve upon this by removing some NPM packages that are not necessarily needed
