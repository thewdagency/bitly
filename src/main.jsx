/**
 * main.js
 *
 * This is the main compiler file that is a Webpack entrypoint used
 * to bundle all required assets together into single file(s).
 */
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import { BrowserRouter } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme, responsiveFontSizes } from '@mui/material/styles';
import { themeObj } from './styles/material/themeObj';
import App from './components/App';

let theme = createTheme(themeObj);
theme = responsiveFontSizes(theme);

ReactDOM.render(
  (
    <BrowserRouter>
      <CssBaseline enableColorScheme />
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  ),
  document.getElementById('root')
);
