import React from 'react'
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import SearchBar from "../components/Search/SearchBar";

const Home = (props) => (
  <Grid container spacing={3}>
    <Grid item sm={12} sx={{
      my: ['2rem', '3rem', '6rem'],
      textAlign: ['center']
    }}>
      <Typography variant="h1" component="h1" sx={{
        fontWeight: 900,
        color: 'text.primary'
      }}>
        SHORTEN. SHARE. MEASURE.
      </Typography>
      <Typography variant="body1" paragraph sx={{
        fontSize: 'fontSize',
        pt: '1rem',
        pb: '1.6rem',
        color: 'text.primary'
      }}>
        Join Bitly, the world's leading link management platform.
      </Typography>

      <SearchBar />
    </Grid>
  </Grid>
)

export default Home;
