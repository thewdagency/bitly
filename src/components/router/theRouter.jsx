import React, { Suspense } from 'react';
import {
  Routes,
  Route
} from 'react-router-dom';
import Home from '../../pages/Home';
import PageLoading from '../loading/PageLoading';
import NoMatch from '../../pages/NoMatch';

const TheRouter = () => (
  <Suspense fallback={<PageLoading />}>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="*" element={<NoMatch />} />
    </Routes>
  </Suspense>
);

export default TheRouter;
