/**
 * Snackbar
 */

import React, { useEffect, useState } from 'react'
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

const BitlySnackbar = ({
    showSnackbar,
    setShowSnackbar,
    message,
    showCloseIcon
}) => {
    const closeSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setShowSnackbar(false);
    };

    const snackAction = (
        <>
            {showCloseIcon && (
                <IconButton
                    size="small"
                    aria-label="close"
                    color="inherit"
                    onClick={closeSnackbar}
                >
                    <CloseIcon fontSize="small" />
                </IconButton>
            )}
        </>
    )

    return (
        <Snackbar
            open={showSnackbar}
            autoHideDuration={6000}
            onClose={closeSnackbar}
            message={message}
            action={snackAction}
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        />
    )
}

export default BitlySnackbar;
