/* eslint import/prefer-default-export: 0 */
/* eslint no-console: 0 */

/**
 * Format bytes
 *
 * @param bytes
 * @param decimals
 * @returns {string}
 */
const formatBytes = (bytes, decimals) => {
  if (bytes === 0) {
    return '0';
  }

  const k = 1024;
  const dm = decimals <= 0 ? 0 : decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / (k ** i)).toFixed(dm))} ${sizes[i]}`;
};

/**
 * Get full formatted date using toLocaleString.
 * You can provide a language and options for formatting.
 *
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
 *
 * @param createdAt
 * @param options
 * @param lang
 * @returns {string}
 */
const getFormattedDate = (createdAt, options = {}, lang = 'en-US') => {
  if (Object.prototype.toString.call(createdAt) !== '[object Date]') {
    const theDate = new Date(createdAt.toDate());
    return theDate.toLocaleString(lang, options);
  }

  return createdAt.toLocaleString(lang, options);
};

/**
 * Sorting objects with 'createdAt' value
 *
 * @param a
 * @param b
 * @returns {number}
 */
const sortCreatedAt = (a, b) => {
  if (new Date(a.createdAt.toDate()) > new Date(b.createdAt.toDate())) {
    return -1;
  }

  if (new Date(a.createdAt.toDate()) < new Date(b.createdAt.toDate())) {
    return 1;
  }

  return 0;
};

/**
 * Check for URL validity
 *
 * @param url
 * @returns {boolean}
 */
const isValidURL = (url) => {
  return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(url);
};

/**
 Data Storage Helpers

 Manage connections related to data storage such as requirements for sessionstorage
 and/or cookie management. When instantiating the class it will by default use
 sessionstorage. You can pass values to the constructor if needing to utilize
 cookie storage instead. Be mindful of the storage size limitations.
 */
class DataStorage {
  constructor(cookie = false, cookieExpDays = 1) {
    this.cookie = cookie;
    this.cookieExpireDays = cookieExpDays;
    this.dataName = '';
    this.dataval = '';
  }

  getCookieStorage = () => {
    const name = `${this.dataName}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i += 1) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }

    return '';
  };

  /**
   * Sets and stores the data to a cookie based upon the passed values
   */
  setCookieStorage = () => {
    const today = new Date();
    today.setTime(today.getTime() + (this.cookieExpireDays * 24 * 60 * 60 * 1000));
    const expires = `expires=${today.toUTCString()}`;
    document.cookie = `${this.dataName}=${this.dataVal};${expires};path=/`;
  };

  /**
   * Removes the cookie storage
   */
  removeCookieStorage = () => {
    this.dataVal = this.getCookieStorage();
    document.cookie = `${this.dataName}=${this.dataVal};expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/`;
  }

  /**
   * Sets and stores the data to sessionstorage based upon the passed values
   *
   * @param dataName
   * @param dataVal
   */
  setStorageData = (dataName, dataVal) => {
    this.dataName = dataName;
    this.dataVal = dataVal;

    if (this.checkStorageCapabilities() && !this.cookie) {
      this.setSessionStorage();
    } else {
      this.setCookieStorage();
    }
  };

  /**
   * Returns the stored data based upon the dataName
   *
   * @param dataName
   * @returns {string|string}
   */
  getStorageData = (dataName) => {
    this.dataName = dataName;
    this.dataVal = '';

    if (this.checkStorageCapabilities() && !this.cookie) {
      return this.getSessionStorage();
    }

    return this.getCookieStorage();
  };

  /**
   * Removes the storage data
   *
   * @param dataName
   */
  removeStorageData = (dataName) => {
    this.dataName = dataName;
    this.dataVal = '';

    if (this.checkStorageCapabilities() && !this.cookie) {
      return this.removeSessionStorage();
    }

    return this.removeCookieStorage();
  }

  getSessionStorage = () => sessionStorage.getItem(this.dataName);

  setSessionStorage = () => sessionStorage.setItem(this.dataName, this.dataVal);

  removeSessionStorage = () => sessionStorage.removeItem(this.dataName);

  /**
   * Check storage capabilities. Returns true if sessionstorage is available, false if not.
   */
  checkStorageCapabilities = () => {
    try {
      sessionStorage.setItem('test', 'test');
      sessionStorage.removeItem('test');
      return true;
    } catch (e) {
      return false;
    }
  };
}

export {
  formatBytes,
  getFormattedDate,
  sortCreatedAt,
  isValidURL,
  DataStorage
};
