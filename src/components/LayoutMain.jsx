import React from 'react';
// import { useLocation } from 'react-router-dom';
import Box from '@mui/material/Box';
import Header from './header/header';
import FooterMain from './footer/FooterMain';
import bgImage from '../images/background-image.png';
import '../styles/_scss/main.scss';

const LayoutMain = ({ children }) => {
  // const location = useLocation();

  return (
    <Box sx={{
      backgroundImage: ['none', 'none', `url(${bgImage})`],
      backgroundPosition: 'top center',
      backgroundRepeat: 'no-repeat',
      backgroundColor: ['#6db5df', '#6db5df', '#fff'],
      backgroundSize: 'cover',
      maxHeight: ['auto', 'auto', '600px']
    }}>
      <Header />

      <div role="main">
        {children}
      </div>

      <FooterMain />
    </Box>
  );
}

export default LayoutMain;
