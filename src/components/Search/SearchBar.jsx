import React, { useState } from 'react'
import { v1 as uuid } from 'uuid';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import BitlySnackbar from '../notifications/BitlySnackbar';
import BitlySDK from '../sdk';
import { DataStorage, isValidURL } from '../helpers';
import Typography from "@mui/material/Typography";

// Instantiate the storage instance
const DS = new DataStorage();
const storedLinkDataName = 'bitly-link-test-data';

/**
 * setToStorage
 *
 * Handles saving the data to the session/cookie
 */
const setToStorage = (linkData) => {
  const currentData = DS.getStorageData(storedLinkDataName);

  if (!currentData) {
    const linkDataArray = [];
    linkDataArray.push(linkData);
    DS.setStorageData(storedLinkDataName, JSON.stringify(linkDataArray));
  } else {
    const currentDataParsed = JSON.parse(currentData);
    currentDataParsed.push(linkData);

    DS.setStorageData(storedLinkDataName, JSON.stringify(currentDataParsed));
  }
}

const ShowLinkInfo = () => {
  const currentData = DS.getStorageData(storedLinkDataName);

  if (currentData) {
    const parsedData = JSON.parse(currentData);

    return (
      <Container maxWidth="lg" sx={{ padding: 0 }}>
        <Grid container sx={{
          backgroundColor: '#fff',
          padding: '1rem',
          width: ['100%', '85%'],
          margin: '2rem auto',
          borderRadius: '3px',
          boxShadow: '1px 1px 1px 2px #f7f7f7'
        }}>
          {parsedData.map((linkData, index) => (
            <Grid key={uuid()} item xs={12} sx={{
              textAlign: 'left',
              color: '#444444'
            }}>
              <Typography variant="body1">
                {linkData.urlTitle}
              </Typography>
              <Typography variant="body2">
                {linkData.fullUrl}
              </Typography>

              <Grid container>
                <Grid item xs={12} sm={6} sx={{
                  textAlign: ['center', 'left']
                }}>
                  <Typography variant="body1">
                    <a href={linkData.shortenedUrl} target="_blank" rel="noopener nofollow" style={{
                      color: '#ee6123'
                    }}>{linkData.shortenedUrl}</a>
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6} sx={{
                  textAlign: ['center', 'right']
                }}>
                  <Typography variant="body2">
                    {linkData.urlClicks}
                  </Typography>
                </Grid>
              </Grid>

              {/* Only add HR if not first or last row of data */}
              {(parsedData.length !== 1 || index !== 0) && index !== parsedData.length - 1 && (
                  <hr />
              )}
            </Grid>
          ))}
        </Grid>
      </Container>
    )
  }

  return '';
}

const SearchBar = (props) => {
  const [urlValue, setUrlValue] = useState('');
  const [updateLinkData, setUpdateLinkData] = useState('');
  const [showSnackbar, setShowSnackbar] = useState(false);
  const [snackMessage, setSnackMessage] = useState('');

  // Setup Bitly SDK
  const bitlySDK = new BitlySDK({
    accessToken: process.env.BITLY_ACCESS_TOKEN
  });

  const handleUrlChange = (e) => {
    setUrlValue(e.target.value);
  }

  const handleUrlSubmit = (e) => {
    e.preventDefault();

    // Check value of input box and make sure is a url
    if (isValidURL(urlValue)) {
      const urlData = {};

      // bitlySDK.expand('https://bit.ly/3oGIIKj/').then((expanded) => console.log(expanded));

      // ADDING TEST DATA
      const testLinkData = {shortenedUrl: 'https://bit.ly/faker', fullUrl: urlValue, urlTitle: `Sample Title for ${urlValue}`, urlClicks: Math.floor(Math.random() * (1000 - 100 + 1) + 100)};
      setToStorage(testLinkData);

      // Would probably use something like this to show a wait icon or something
      setTimeout(() => {
        setUpdateLinkData(testLinkData);
      }, 500);

      // API NOT FUNCTIONING, USE DATA FROM ABOVE FUNCTION
      //
      // // Go through the Bitly promises to save the link data
      // bitlySDK.shorten(urlValue)
      //   .then((result) => {
      //     // console.log('SHORTEN:', result);
      //     urlData.shortenedUrl = result.url;
      //     urlData.fullUrl = result.longUrl;
      //     return bitlySDK.info(result.url);
      //   })
      //   .then((result2) => {
      //     // console.log('INFO:', result2);
      //     urlData.urlTitle = result2.title;
      //     return bitlySDK.clicks(urlData.shortenedUrl);
      //   })
      //   .then((result3) => {
      //     // console.log('CLICKS:', result3);
      //     urlData.urlClicks = result3[0].global_clicks;
      //
      //     // Set this data to storage
      //     setToStorage(urlData);
      //   })
      //   .catch((err) => {
      //     setSnackMessage(`${err}`);
      //     setShowSnackbar(true);
      //   });
    } else {
      setSnackMessage(`${urlValue} is not a valid URL`);
      setShowSnackbar(true);
    }
  }

  return (
    <Container maxWidth="lg">
      <Grid container>
        <Grid item xs={12} sx={{
          textAlign: ['center']
        }}>
          <form noValidate autoComplete="off">
            <TextField
              id="linkTextField"
              variant="filled"
              required
              value={urlValue}
              onChange={handleUrlChange}
              placeholder="Paste a link to shorten it"
              InputProps={{ disableUnderline: true }}
              inputProps={{
                style: {
                  color: '#444444',
                  padding: '1.1rem',
                  fontSize: 'fontSize',
                  borderTopLeftRadius: ['3px'],
                  borderBottomLeftRadius: ['3px'],
                  borderTopRightRadius: ['3px', 0],
                  borderBottomRightRadius: ['3px', 0]
                }
              }}
              sx={{
                backgroundColor: 'background.default',
                width: ['100%', '70%'],
                borderTopLeftRadius: ['3px'],
                borderBottomLeftRadius: ['3px'],
                borderTopRightRadius: ['3px', 0],
                borderBottomRightRadius: ['3px', 0],
                '& .MuiFilledInput-root': {
                  backgroundColor: 'background.default',
                  '&:hover': {
                    'input': {
                      backgroundColor: 'background.default'
                    }
                  },
                  'input': {
                    backgroundColor: 'background.default',
                    border: '2px solid #fff',
                    '&:focus': {
                      border: '2px solid #ee6123',
                    },
                    '&:hover': {
                      backgroundColor: 'background.default',
                    }
                  }
                }
              }}
            />
            <Button type="submit" onClick={handleUrlSubmit} variant="contained" color="primary" size="large" sx={{
              fontSize: '13px',
              fontWeight: '600',
              padding: '22px 34px 21px',
              borderTopLeftRadius: ['3px', 0],
              borderBottomLeftRadius: ['3px', 0],
              mt: ['1.5rem', 0],
              boxShadow: 'none'
            }}>
              SHORTEN
            </Button>
          </form>
        </Grid>
      </Grid>

      <ShowLinkInfo />

      {showSnackbar && (
          <BitlySnackbar showSnackbar setShowSnackbar={setShowSnackbar} message={snackMessage} showCloseIcon />
      )}
    </Container>
  )
}

export default SearchBar;
