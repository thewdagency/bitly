/**
 * toggleClass
 *
 * Toggle a class name for a specific element
 */

const toggleClass = (el, className) => {
  if (!el) {
    throw new Error('Element requested to toggle does not exist');
  }

  if (el.classList.contains(className)) {
    el.classList.remove(className);
  } else {
    el.classList.add(className);
  }
};

export default toggleClass;
