import React, { Component } from 'react';
import _throttle from 'lodash/throttle';
import { Link, animateScroll as scroll } from 'react-scroll';

/* eslint class-methods-use-this: 0 */

class ScrollToTop extends Component {
  componentDidMount() {
    // Set scroll listener to see how far down the page we are.
    // If we are low enough, show the scrollToTop icon
    document.addEventListener('scroll', _throttle(() => {
      const scrollToTop = document.querySelector('.scroll-to-top');

      scrollToTop.style.display = window.scrollY >= 150 ? 'block' : 'none';
    }, 500));
  }

  scrollToTop = (e) => {
    e.preventDefault();
    scroll.scrollToTop();
  };

  render() {
    return (
      <a className="scroll-to-top rounded" href="#" onClick={this.scrollToTop}>
        <span className="fas fa-angle-up" />
      </a>
    );
  }
}

export default ScrollToTop;
