/**
 * sliding
 *
 * Utility to handle sliding animations
 */
import './toggleSlide.scss';

export default class ToggleSlide {
  /**
   * Constructor.
   *
   * @param el
   */
  constructor(el) {
    this.theElement = el;
    // Check current height and slide accordingly
    if (this.theElement.classList.contains('show')) {
      this.theElement.classList.remove('show');
    } else {
      this.theElement.classList.add('show');
    }
    // Check aria-expanded and update
    if (this.theElement.hasAttribute('aria-expanded')) {
      if (this.theElement.getAttribute('aria-expanded') === 'false') {
        this.theElement.setAttribute('aria-expanded', 'true');
      } else {
        this.theElement.setAttribute('aria-expanded', 'false');
      }
    }
  }
}
