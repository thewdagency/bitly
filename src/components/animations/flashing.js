/**
 * flashing
 *
 * Handle flashing animations.
 *
 * INSTRUCTIONS:
 * In order to use this you will need to add a class named 'show' to
 * the CSS of the receiving element. This will contain the opposite
 * transition effect of the element. For example:
 *
 * .scroll-down-arrow {
 *    opacity: 0;
 *    transition: 1.5s opacity;
 *
 *    &.show {
 *      opacity: 1;
 *    }
 *  }
 *
 * Once you have the CSS setup you can call the 'flashing' function
 * with three arguments:
 *
 *    element    - The ID or class of the receiving element (if a class, include the period)
 *    duration   - Duration of flash in milliseconds
 *    numFlashes - Total number of flashes, leave blank if going to flash forever
 */

const flashing = (element, duration, numFlashes) => {
  let elToFlash = null;

  // Set element depending on if the element is a class or id
  if (element.indexOf('.') >= 0) {
    elToFlash = document.querySelector(element);
  } else {
    elToFlash = document.getElementById(element);
  }

  // Start setting the interval data
  let flashInterval = null;
  let timesToFlash = numFlashes !== 'undefined' ? numFlashes : 'infinite';

  const doFlash = () => {
    if (timesToFlash !== 'infinite') {
      timesToFlash -= 1;
    }

    // Show or hide depending on current 'show' class
    if (elToFlash.classList.contains('show')) {
      elToFlash.classList.remove('show');
    } else {
      elToFlash.classList.add('show');
    }

    if (timesToFlash <= 0) {
      clearInterval(flashInterval);
    }
  };

  flashInterval = setInterval(doFlash, duration);
};

export default flashing;
