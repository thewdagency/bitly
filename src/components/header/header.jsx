import React from 'react';
import Box from '@mui/material/Box';
import MainNav from '../nav/mainNav';

const Header = props => (
  <Box sx={{ padding: '1rem' }}>
    <MainNav />
  </Box>
);

export default Header;
