import React from 'react';
import Routes from './router/theRouter';
import LayoutMain from './LayoutMain';

const App = props => (
  <LayoutMain>
    <Routes />
  </LayoutMain>
);

export default App;
