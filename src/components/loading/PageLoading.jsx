import React from 'react';
import Box from '@mui/material/Box';
import LoopIcon from '@mui/icons-material/Loop';

const PageLoading = () => (
  <Box component="section" sx={{
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
    width: '100%',
    minHeight: '100vh',
    backgroundColor: '#9a9a9a',
  }}>
    <LoopIcon sx={{
      fontSize: '4rem',
      animation: 'spin 2s linear infinite',
      '@keyframes spin': {
        '0%': {
          transform: 'rotate(360deg)'
        },
        '100%': {
          transform: 'rotate(0deg)'
        }
      },
      color: '#6f6f6f',
    }} />
  </Box>
);

export default PageLoading;
