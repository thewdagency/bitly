import React from 'react';
import Box from '@mui/material/Box';
import LoopIcon from '@mui/icons-material/Loop';

const ContentLoading = props => (
  <Box component="section" sx={{ textAlign: 'center' }}>
    <LoopIcon sx={{
      animation: 'spin 2s linear infinite',
      '@keyframes spin': {
        '0%': {
          transform: 'rotate(360deg)'
        },
        '100%': {
          transform: 'rotate(0deg)'
        }
      }
    }} />
  </Box>
);

export default ContentLoading;
