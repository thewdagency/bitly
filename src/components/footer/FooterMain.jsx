import React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

const FooterMain = props => {
  const d = new Date();
  let year = d.getFullYear();

  return (
    <Box component="footer" sx={{
      backgroundColor: '#fff',
      padding: '3rem 2rem',
    }}>
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item sm={12} sx={{
            fontSize: '0.8rem',
            textAlign: ['center', 'right']
          }}>
            <Typography>
              Copyright &#169; {year} Bitly, Inc. All rights reserved.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default FooterMain;
