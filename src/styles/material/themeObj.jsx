/**
 * themeObj
 *
 * Build the theme object required for setting up the default
 * settings for the theme using material-ui.
 *
 * DEFAULTS: https://material-ui.com/customization/default-theme/
 */


/**
 * Convert pixels to REM value
 *
 * @param value
 * @returns {string}
 */
const pxToRem = value => (`${value / 16}rem`);

const themeObj = (
  {
    palette: {
      background: {
        default: '#fff'
      },
      text: {
        primary: '#fff',
        secondary: '#444444'
      },
      primary: {
        main: '#ee6123' // Orange
      },
      secondary: {
        main: '#444444' // Dark Gray
      }
    },
    // status: {
    //   danger: 'red'
    // },
    typography: {
      // useNextVariants: true,
      fontFamily: 'Helvetica, sans-serif',
      fontSize: 16,
      color: '#000000',
      body1: {
        color: '#444444'
      },
      body2: {
        color: '#9b9b9b'
      },
      h1: {
        fontSize: pxToRem(58),
        lineHeight: 1,
        letterSpacing: '0',
      },
      h2: {
        fontSize: pxToRem(31),
        fontWeight: 600,
        lineHeight: 1,
        letterSpacing: '0'
      },
      h3: {
        fontSize: pxToRem(28),
        lineHeight: 1
      },
    },
    components: {

    },
  }
);

export {
  themeObj
};
