const fse = require('fs-extra');

// Copy files from front end source to dist folder
// fse.copy('./src/fonts', './dist/src/fonts')
//   .then(() => {
//     // Copy all assets to theme dir
//     fse.copy('./src/images', './dist/assets/images')
//       .then(() => console.log('Fonts and Images Moved Successfully!'))
//       .catch(err => console.error(err));
//   })
//   .catch(err => console.error(err));

fse.copy('./src/static', './dist/static')
  .then(() => console.log('Static Folder Moved Successfully!'))
  .catch(err => console.error(err));
