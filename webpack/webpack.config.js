const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Dotenv = require('dotenv-webpack')
const { exec } = require('child_process')

var config = {
  entry: {
    main: './src/main'
  },
  output: {
    path: path.join(__dirname, '../dist'),
    publicPath: '/',
    filename: 'src/scripts/[name].js',
    clean: true
  },
  // webpack 5 comes with devServer which loads in development mode
  devServer: {
    port: 3000,
    server: 'https',
    historyApiFallback: true,
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /(\.scss)$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader' },
          {
            loader: 'resolve-url-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: ['src/styles/_scss/_variables.scss'],
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(pdf)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              fallback: 'file-loader',
              name: '[path][name].[ext]',
              context: './src/files',
              outputPath: './src/files',
              limit: 10000
            }
          },
          {
            loader: 'img-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|psd|mp4|ico|ics)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              fallback: 'file-loader',
              name: '[path][name].[ext]',
              context: './src/images',
              outputPath: './src/images',
              limit: 10000
            }
          },
          {
            loader: 'img-loader'
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              fallback: 'file-loader',
              name: '[path][name].[ext]',
              context: './src/fonts',
              outputPath: './src/fonts',
              limit: 10000
            }
          },
          {
            loader: 'img-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    {
      apply: (compiler) => {
        compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {
          exec('node ./webpack/prodPostCompile.js', (err, stdout, stderr) => {
            if (stdout) process.stdout.write(stdout);
            if (stderr) process.stderr.write(stderr);
          });
        });
      }
    },
    new MiniCssExtractPlugin({
      filename: 'src/styles/styles.css'
    }),
  ],
}

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'inline-source-map';

    config.plugins.unshift(new Dotenv());
  }

  if (argv.mode === 'production') {
    // config.devtool = 'source-map'; // Enable for troubleshooting source

    config.plugins.unshift(new Dotenv({ systemvars: true }));
  }

  return config;
};
